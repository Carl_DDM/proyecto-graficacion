/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control_del_servidor;

/**
 *
 * @author Brando
 */
public class Jugador {

    private String nombre;
    private int credito, ganado, perdido;
    private boolean jugando;

    public Jugador(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public int getCredito() {
        return credito;
    }

    public int getGanado() {
        return ganado;
    }

    public int getPerdido() {
        return perdido;
    }

    public boolean isJugando() {
        return jugando;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCredito(int credito) {
        this.credito = credito;
    }

    public void setGanado(int ganado) {
        this.ganado = ganado;
    }

    public void setPerdido(int perdido) {
        this.perdido = perdido;
    }

    public void setJugando(boolean jugando) {
        this.jugando = jugando;
    }

}
